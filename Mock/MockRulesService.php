<?php

namespace CloudDistrict\ReduxBundle\Mock;

use CloudDistrict\ReduxBundle\Document\Dispatchable;
use CloudDistrict\ReduxBundle\Document\Rule;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use CloudDistrict\ReduxBundle\Interfaces\RulesServiceInterface;

class MockRulesService implements RulesServiceInterface {

    protected $expressionLanguage;

    function __construct() {
        $this->expressionLanguage = new ExpressionLanguage();
    }

    public function findRulesByType($type) {
        $rule = new Rule();
        $rule->setType('postcondition');
        $rule->setExpression('element.getLastAction()== "test"');
        $rule->setNextAction("doScoring");
        $rule->setNextActionParams(array('scoring' => "test"));
        $rule->setAction(array('action' => 'launchEmail', 'params' => array('template' => 1)));

        return array($rule);
    }

    public function findRulesByTypeAndAction($type, $action) {
        return array();
    }

    public function evaluateRule(Rule $rule, Dispatchable $dispatchable) {
        return $this->expressionLanguage->evaluate($rule->getExpression(), array('element' => $dispatchable));
    }

}
