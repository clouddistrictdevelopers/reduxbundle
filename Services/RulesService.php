<?php

namespace CloudDistrict\ReduxBundle\Services;

use CloudDistrict\ReduxBundle\Document\Dispatchable;
use CloudDistrict\ReduxBundle\Document\Rule;
use Symfony\Component\ExpressionLanguage\ExpressionLanguage;
use CloudDistrict\ReduxBundle\Interfaces\RulesServiceInterface;

class RulesService implements RulesServiceInterface {

    protected $expressionLanguage;
    protected $dm;

    function __construct($dm) {
        $this->dm = $dm;
        $this->expressionLanguage = new ExpressionLanguage();
    }

    public function findRulesByType($type) {
        return $this->dm->createQueryBuilder('CloudDistrictReduxBundle:Rule')
                        ->field('type')->equals($type)
                        ->getQuery()
                        ->execute();
    }

    public function findRulesByTypeAndAction($type, $action) {
        return $this->dm->createQueryBuilder('CloudDistrictReduxBundle:Rule')
                        ->field('type')->equals($type)
                        ->field('action')->equals($action)
                        ->getQuery()
                        ->execute();
    }

    public function evaluateRule(Rule $rule, Dispatchable $dispatchable) {
        return $this->expressionLanguage->evaluate($rule->getExpression(), array('element' => $dispatchable));
    }

}
