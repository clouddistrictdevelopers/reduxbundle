<?php

namespace CloudDistrict\ReduxBundle\Services;

use CloudDistrict\ReduxBundle\Document\Dispatchable;
use CloudDistrict\ReduxBundle\Exception\WorkflowException;
use CloudDistrict\ReduxBundle\Interfaces\PostActionHandlerInterface;

class BasePostActionHandler implements PostActionHandlerInterface{

    public function handle(Dispatchable $dispatchable, $postAction, $params = array(), &$errors = array()) {

        return array('state' => $dispatchable, 'errors' => $errors);
    }

    public static function getPostActions() {
        return array(
           array('action'=>"testaction", 'params'=>array('param1','param2'))
        );
    }

}
