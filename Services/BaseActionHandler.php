<?php

namespace CloudDistrict\ReduxBundle\Services;

use CloudDistrict\ReduxBundle\Document\Dispatchable;
use CloudDistrict\ReduxBundle\Exception\WorkflowException;
use CloudDistrict\ReduxBundle\Interfaces\ActionHandlerInterface;

class BaseActionHandler implements ActionHandlerInterface{

    public function handle(Dispatchable $dispatchable, $action, $params = array(), &$errors = array()) {

        $dispatchable->setLastAction($action);
        
        return array('state' => $dispatchable, 'errors' => $errors);
    }

}
