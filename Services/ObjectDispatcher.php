<?php

namespace CloudDistrict\ReduxBundle\Services;

use Symfony\Component\DependencyInjection\ContainerAware;
use CloudDistrict\ReduxBundle\Document\Dispatchable;
use CloudDistrict\ReduxBundle\Exception\WorkflowException;
use CloudDistrict\ReduxBundle\Interfaces\ActionHandlerInterface;
use CloudDistrict\ReduxBundle\Interfaces\PostActionHandlerInterface;
use CloudDistrict\ReduxBundle\Interfaces\RulesServiceInterface;

class ObjectDispatcher {

    protected $rules;
    protected $actionHandler;
    protected $postActionHandler;

    function __construct(ActionHandlerInterface $actionHandler, PostActionHandlerInterface $postActionHandler, RulesServiceInterface $rules) {
        $this->actionHandler = $actionHandler;
        $this->postActionHandler = $postActionHandler;
        $this->rules = $rules;
    }

    public function dispatch(Dispatchable $dispatchable, $action, $params = array(), &$errors = array()) {

        ////////////////////////
        // preconditions
        ////////////////////////

        $rules = $this->rules->findRulesByTypeAndAction('precondition', $action);

        // evaluate preconditions

        $invalidRules = array();
        if (count($rules) != 0) {
            foreach ($rules as $rule) {
                if (!$this->rules->evaluateRule($rule, $dispatchable)) {
                    $invalidRules[] = array('expression' => $rule->getExpression(), 'id' => $rule->getId());

                }
            }
        }
        
        if(count($invalidRules)){
            return array('state' => $dispatchable, 'errors' => $invalidRules);
        }

        // handle action 
        $result = $this->actionHandler->handle($dispatchable, $action, $params, $errors);

        // return if errors
        if (count($errors) > 0) {
            return array('errors' => $errors);
        }

        ////////////////////////
        // postconditions
        ////////////////////////

        $rules = $this->rules->findRulesByType('postcondition');


        foreach ($rules as $rule) {

            if ($this->rules->evaluateRule($rule, $dispatchable)) {

                //post actions (email,sms,etc)
                foreach ($rule->getPostActions() as $action) {
                    $this->postActionHandler->handle($dispatchable, $action['action'], $action['params'], $errors);
                }
                // launch next state action
                if ($rule->getNextAction()) {
                    
                    $this->dispatch($dispatchable, $rule->getNextAction(), $rule->getNextActionParams(), $errors);
                }
            }
        }


        return array('state' => $dispatchable, 'errors' => $errors);
    }

}
