<?php

namespace CloudDistrict\ReduxBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 * @MongoDB\MappedSuperclass 
*/
class Dispatchable {

    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\Collection
     */
    private $actions = array();

    /**
     * @MongoDB\String
     */
    private $lastAction;

    function getId() {
        return $this->id;
    }

    function getActions() {
        return $this->actions;
    }

    function getLastAction() {
        return $this->lastAction;
    }

    function setActions($actions) {
        $this->actions = $actions;
    }

    function setLastAction($lastAction) {
        $this->actions[] = array('action' => $lastAction, 'timestmap' => new \DateTime());
        $this->lastAction = $lastAction;
    }

}
