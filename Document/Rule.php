<?php

namespace CloudDistrict\ReduxBundle\Document;

use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * @MongoDB\Document
 */
class Rule {

    /**
     * @MongoDB\Id
     */
    private $id;

    /**
     * @MongoDB\String
     */
    private $type;

    /**
     * @MongoDB\String
     */
    private $action;
    
    /**
     * @MongoDB\String
     */
    private $expression;

    /**
     * @MongoDB\Collection
     */
    private $postActions = array();
    
    /**
     * @MongoDB\String
     */
    private $nextAction;
    
        /**
     * @MongoDB\Collection
     */
    private $nextActionParams=array();

    function getId() {
        return $this->id;
    }

    function getExpression() {
        return $this->expression;
    }



    function getType() {
        return $this->type;
    }

    function setType($type) {
        $this->type = $type;
    }
    
    function setExpression($expression) {
        $this->expression = $expression;
    }

    function getAction() {
        return $this->action;
    }

    function setAction($action) {
        $this->action = $action;
    }

    function getPostActions() {
        return $this->postActions;
    }

    function getNextAction() {
        return $this->nextAction;
    }

    function getNextActionParams() {
        return $this->nextActionParams;
    }

    function setPostActions($postActions) {
        $this->postActions = $postActions;
    }

    function setNextAction($nextAction) {
        $this->nextAction = $nextAction;
    }

    function setNextActionParams($nextActionParams) {
        $this->nextActionParams = $nextActionParams;
    }




}
