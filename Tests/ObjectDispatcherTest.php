<?php

namespace CloudDistrict\ReduxBundle\Tests;

use Doctrine\ODM\MongoDB\Tests\Mocks\DocumentManagerMock;
use Doctrine\ODM\MongoDB\Tests\Mocks\ConnectionMock;
use CloudDistrict\ReduxBundle\Services\ObjectDispatcher;
use CloudDistrict\ReduxBundle\Services\BaseActionHandler;
use CloudDistrict\ReduxBundle\Services\BasePostActionHandler;
use CloudDistrict\ReduxBundle\Services\RulesService;
use CloudDistrict\ReduxBundle\Document\Rule;
use CloudDistrict\ReduxBundle\Document\Dispatchable;
use CloudDistrict\ReduxBundle\Mock\MockRulesService;

class ObjectDispatcherTest extends \PHPUnit_Framework_TestCase {

    protected function setUp() {
        parent::setUp();
    }

    public function testDispatch() {


        $this->assertTrue(true);

        $actionHandler = new BaseActionHandler();
        $postActionHandler = new BasePostActionHandler();

        $rulesService = $this->getMockBuilder('CloudDistrict\ReduxBundle\Services\RulesService')
                        ->disableOriginalConstructor()->getMock();
        $rulesService->expects($this->any())
                ->method('findRulesByType')
                ->will($this->returnValue(array()));

        $rulesService->expects($this->any())
                ->method('findRulesByActionAndType')
                ->will($this->returnValue(array()));


        $objectDispatcher = new ObjectDispatcher($actionHandler, $postActionHandler, $rulesService);

        $dispatchable = new Dispatchable();
        $action = "test";
        $params = array();
        $errors = array();
        $result = $objectDispatcher->dispatch($dispatchable, $action, $params, $errors);

        $this->assertEquals($result['state']->getLastAction(), "test");
        $this->assertEquals(count($result['errors']), 0);
    }

    public function testDispatchWithJump() {


        $this->assertTrue(true);

        $actionHandler = new BaseActionHandler();
        $postActionHandler = new BasePostActionHandler();

        $rulesService = new MockRulesService();



        $objectDispatcher = new ObjectDispatcher($actionHandler, $postActionHandler, $rulesService);

        $dispatchable = new Dispatchable();
        $action = "test";
        $params = array();
        $errors = array();
        $result = $objectDispatcher->dispatch($dispatchable, $action, $params, $errors);

        $this->assertEquals($dispatchable->getLastAction(), "doScoring");
        $this->assertEquals(count($result['errors']), 0);
        $this->assertEquals(count($dispatchable->getActions()), 2);
    }

}
