<?php

namespace CloudDistrict\ReduxBundle\Interfaces;

use CloudDistrict\ReduxBundle\Document\Dispatchable;

interface PostActionHandlerInterface{
     public function handle(Dispatchable $dispatchable, $action, $params = array(), &$errors = array());
     public static function getPostActions();
}
