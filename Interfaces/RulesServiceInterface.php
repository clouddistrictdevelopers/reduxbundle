<?php

namespace CloudDistrict\ReduxBundle\Interfaces;

use CloudDistrict\ReduxBundle\Document\Dispatchable;
use CloudDistrict\ReduxBundle\Document\Rule;

interface RulesServiceInterface {

    public function evaluateRule(Rule $rule, Dispatchable $dispatchable);
}
