<?php

namespace CloudDistrict\ReduxBundle\Interfaces;

use CloudDistrict\ReduxBundle\Document\Dispatchable;

interface ActionHandlerInterface{
     public function handle(Dispatchable $dispatchable, $action, $params = array(), &$errors = array());

}
