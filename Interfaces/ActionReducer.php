<?php

namespace CloudDistrict\ReduxBundle\Interfaces;

interface ActionReducer{
    public function getName();

}
