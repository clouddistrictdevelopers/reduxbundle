<?php

namespace CloudDistrict\ReduxBundle\DependencyInjection
;
use Symfony\Component\Config\Definition\Builder\ArrayNodeDefinition;
use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;
class Configuration implements ConfigurationInterface
{
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('cloud_district_redux');
        $this->addConfigSection($rootNode);
        return $treeBuilder;
    }
    private function addConfigSection(ArrayNodeDefinition $rootNode)
    {
        $rootNode
            ->children()
                ->integerNode('max_dispatcher_executions')->defaultValue('10')->end()
            ->end()
        ;
    }
}